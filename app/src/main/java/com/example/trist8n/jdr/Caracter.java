package com.example.trist8n.jdr;
//This is not openSource,if you want to use this source for publishing on store contact me on Trist8n.56@hotmail.fr.
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

public class Caracter {

    private String nom;
    private String race;
    private int level;
    private ArrayList<String> caractere;
    private String element;
    private int psy;
    private int psyMax;
    private int pv;
    private int pvMax;
    private int physique;
    private int social;
    private int mental;
    private int puissance;
    private int finesse;
    private int aura;
    private int relation;
    private int instinct;
    private int savoir;
    private String pathPhoto=null;
    private ArrayList<String> competences;
    private ArrayList<String> dons;
    private ArrayList<String> valeurDons;
    private ArrayList<String> inventaire;
    private ArrayList<String> stuff;


    Caracter(){
        this.pv=this.pvMax;
        this.psy=this.psyMax;
    }

    public ArrayList<String> getCaractere() {
        return caractere;
    }

    public void setCaractere(ArrayList<String> caractere) {
        this.caractere = caractere;
    }

    public ArrayList<String> getInventaire() {
        return inventaire;
    }

    public void setInventaire(ArrayList<String> inventaire) {
        this.inventaire = inventaire;
    }

    public ArrayList<String> getStuff() {
        return stuff;
    }

    public void setStuff(ArrayList<String> stuff) {
        this.stuff = stuff;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getLevel() {
        return level;
    }

    public void setNiveau(int level) {
        this.level = level;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public int getPsy() {
        return psy;
    }

    public int getPsyMax() {
        return psyMax;
    }

    public void setPsyMax(int psyMax) {
        this.psyMax = psyMax;
    }

    public int getPvMax() {
        return pvMax;
    }

    public void setPvMax(int pvMax) {
        this.pvMax = pvMax;
    }

    public void setPsy(int psy) {
        this.psy = psy;
    }

    public int getPv() {
        return pv;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public int getPhysique() {
        return physique;
    }

    public void setPhysique(int physique) {
        this.physique = physique;
    }

    public int getSocial() {
        return social;
    }

    public void setSocial(int social) {
        this.social = social;
    }

    public int getMental() {
        return mental;
    }

    public void setMental(int mental) {
        this.mental = mental;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public int getFinesse() {
        return finesse;
    }

    public void setFinesse(int finesse) {
        this.finesse = finesse;
    }

    public int getAura() {
        return aura;
    }

    public void setAura(int aura) {
        this.aura = aura;
    }

    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }

    public int getInstinct() {
        return instinct;
    }

    public void setInstinct(int instinct) {
        this.instinct = instinct;
    }

    public int getSavoir() {
        return savoir;
    }

    public void setSavoir(int savoir) {
        this.savoir = savoir;
    }

    public ArrayList<String> getCompetences() {
        return competences;
    }

    public void setCompetences(ArrayList<String> competences) {
        this.competences = competences;
    }

    public ArrayList<String> getDons() {
        return dons;
    }

    public void setDons(ArrayList<String> dons) {
        this.dons = dons;
    }

    public ArrayList<String> getValeurDons() {
        return valeurDons;
    }

    public void setValeurDons(ArrayList<String> valeurDons) {
        this.valeurDons = valeurDons;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    @Override
    public String toString() {
        return "Caracter{" +
                "nom='" + nom + '\'' +
                ", race='" + race + '\'' +
                ", level=" + level +
                ", caractere=" + caractere +
                ", element='" + element + '\'' +
                ", psy=" + psy +
                ", psyMax=" + psyMax +
                ", pv=" + pv +
                ", pvMax=" + pvMax +
                ", physique=" + physique +
                ", social=" + social +
                ", mental=" + mental +
                ", puissance=" + puissance +
                ", finesse=" + finesse +
                ", aura=" + aura +
                ", relation=" + relation +
                ", instinct=" + instinct +
                ", savoir=" + savoir +
                ", pathPhoto='" + pathPhoto + '\'' +
                ", competences=" + competences +
                ", dons=" + dons +
                ", valeurDons=" + valeurDons +
                ", inventaire=" + inventaire +
                ", stuff=" + stuff +
                '}';
    }
}
