package com.example.trist8n.jdr;
//This is not openSource,if you want to use this source for publishing on store contact me on Trist8n.56@hotmail.fr.
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreatCaracter extends Activity {
    private LinearLayout creatLayout;
    private ArrayList<String> listePerso;
    private ArrayList<EditText> listEditText;
    private ArrayList<TextView> listTextView;
    private ArrayList<Method> listMethod;
    private ArrayList<Method> listMethodBis;
    private ArrayList<EditText> listTestEditText;
    private boolean testEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_caracter);
        listEditText = new ArrayList<>();
        listTextView = new ArrayList<>();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        final Gson gson = new Gson();

        String jsonList = sharedPreferences.getString("listePerso",null);

        if(jsonList != null){
            listePerso = new ArrayList<>();
            Pattern pattern = Pattern.compile("\"([a-zA-Z])+.([a-zA-Z])+\"");
            Matcher matcher = pattern.matcher(jsonList);
            listePerso = new ArrayList<>();
            while(matcher.find()){
                listePerso.add(jsonList.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1));
            }
            if (!matcher.find()){
                pattern = Pattern.compile("\"([a-zA-Z])\"");
                matcher = pattern.matcher(jsonList);
                while(matcher.find()){
                    listePerso.add(jsonList.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1));
                }
            }
        }else{
            listePerso = new ArrayList<>();
        }
        creatLayout = this.findViewById(R.id.creatLayout);
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.parchemin);
        bg.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);

        creatLayout.setBackgroundDrawable(bg);
        overrideFonts(getApplicationContext(),creatLayout);


        final ArrayList<String> cara = new ArrayList<>();
        final ArrayList<String> compet = new ArrayList<>();
        final ArrayList<String> dons = new ArrayList<>();
        final ArrayList<String> valeurDons = new ArrayList<>();

        final Context ctx = this.getApplicationContext();
        Button btnSave = this.findViewById(R.id.save);

        Class<?> c = null;
        listMethod = new ArrayList<>();
        listMethodBis=new ArrayList<>();
        try {
            c = Caracter.class;
            Object t = c.newInstance();

            Method[] allMethods = c.getDeclaredMethods();
            for (Method m : allMethods) {
                String mname = m.getName();
                if (mname.startsWith("set")) {
                    if(mname.startsWith("setInv")||mname.startsWith("setPath")||mname.startsWith("setStuf")){
                    }
                    else{
                        listMethod.add(m);
                    }
                    continue;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Caracter Perso = new Caracter();
                loadEditText(getApplicationContext(),creatLayout);
                testEditText();
                if (testEditText){
                    for (int i =0;i<listTextView.size();i++) {
                        for (int j=0;j<listMethod.size();j++){
                            Method setMethod = listMethod.get(j);
                            if (setMethod.getName().substring(3,setMethod.getName().length()).equals(listTextView.get(i).getText().toString().substring(0,listTextView.get(i).getText().toString().length()-3))
                                    ||(listTextView.get(i).getText().toString().substring(0,listTextView.get(i).getText().toString().length()-3)+"Max").equals(setMethod.getName().substring(3,setMethod.getName().length()))){
                                if (setMethod.getName().substring(3,setMethod.getName().length()).equals("Pv")||
                                        setMethod.getName().substring(3,setMethod.getName().length()).equals("Psy")){
                                }else{
                                    listMethodBis.add(setMethod);
                                }
                            }
                        }
                    }
                    for (int i=0;i<listMethod.size();i++){
                        Method setMethod = listMethod.get(i);
                        if (setMethod.getName().equals("setValeurDons")){
                            listMethodBis.add(setMethod);
                        }
                    }
                    int find=0;
                    for (int i=0;i<listMethodBis.size();i++){
                        try {
                            listMethodBis.get(i).setAccessible(true);
                            if (listMethodBis.get(i).toGenericString().substring(listMethodBis.get(i).toGenericString().length()-4,listMethodBis.get(i).toGenericString().length()-1).equals("int")) {
                                listMethodBis.get(i).invoke(Perso,Integer.parseInt(listEditText.get(find).getText().toString()));
                            }else if (listMethodBis.get(i).toGenericString().substring(listMethodBis.get(i).toGenericString().length()-7,listMethodBis.get(i).toGenericString().length()-1).equals("String")){
                                listMethodBis.get(i).invoke(Perso,listEditText.get(find).getText().toString());
                            }
                            if (i==3){
                                for (int j=0;j<3;j++){
                                    cara.add(listEditText.get(j+3).getText().toString());
                                }
                                listMethodBis.get(i).invoke(Perso,cara);
                                find+=2;
                            }
                            else if (i==16){
                                for (int j=0;j<5;j++){
                                    compet.add(listEditText.get(j+18).getText().toString());
                                }
                                listMethodBis.get(i).invoke(Perso,compet);
                                find+=4;
                            }
                            else if(i==17){
                                listMethodBis.get(i+1).setAccessible(true);
                                for (int j=0;j<6;j+=2){
                                    dons.add(listEditText.get(j+23).getText().toString());
                                    valeurDons.add(listEditText.get(j+24).getText().toString());
                                }
                                listMethodBis.get(i+1).invoke(Perso,valeurDons);
                                listMethodBis.get(i).invoke(Perso,dons);
                                find+=2;
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        find++;
                    }
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                    Perso.setPsy(Perso.getPsyMax());
                    Perso.setPv(Perso.getPvMax());
                    sharedPreferences.edit()
                            .putString(Perso.getNom(),gson.toJson(Perso))
                            .apply();

                    listePerso.add(Perso.getNom());
                    sharedPreferences.edit()
                            .putString("listePerso",gson.toJson(listePerso))
                            .apply();
                    Intent Test = new Intent(ctx, Play.class);
                    Test.putExtra("namePerso",Perso.getNom());
                    startActivity(Test);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"erreur champs manquant",Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTextSize(30);
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(),"customfont.otf"));
            }
        } catch (Exception e) {
        }
    }
    public void loadEditText(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    loadEditText(context, child);
                }
            } else if (v instanceof EditText ) {
                listEditText.add((EditText) v);
            } else if (v instanceof TextView) {
                if (((TextView) v).getText().toString().equals("Save and Play")){

                }else{
                    listTextView.add((TextView) v);
                }

            }
        } catch (Exception e) {
        }
    }
    public void testEditText(){
        listTestEditText = new ArrayList<>();
        for(int i=0;i<listEditText.size();i++){
            if (listEditText.get(i).getText().toString().equals("")){
                listEditText.get(i).setHintTextColor(Color.RED);
                listTestEditText.add(listEditText.get(i));
            }
        }
        if(listTestEditText.size()==0){
            testEditText=true;
        }else{
            testEditText=false;
        }
    }
}
