package com.example.trist8n.jdr;

//This is not openSource,if you want to use this source for publishing on store contact me on Trist8n.56@hotmail.fr.

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Random;

public class Play extends Activity {

    private static int RESULT_LOAD_IMAGE = 1;
    private Caracter perso;
    private TextView nomPerso;
    private TextView racePerso;
    private TextView levelPerso;
    private TextView elementPerso;
    private TextView pvPerso;
    private TextView manaPerso;
    private TextView physiquePerso;
    private TextView socialPerso;
    private TextView mentalPerso;
    private TextView puissancePerso;
    private TextView finessePerso;
    private TextView auraPerso;
    private TextView relationPerso;
    private TextView instinctPerso;
    private TextView savoirPerso;
    private TextView resBase;
    private LinearLayout inventaire;
    private ImageView btnDecrementLife;
    private ImageView btnDecrementMana;
    private ImageView btnIncrementLife;
    private ImageView btnIncrementMana;
    private ImageView imageDesign;
    private EditText selectionBase;
    private LinearLayout layoutInventaire;
    private LinearLayout layoutStuff;
    private LinearLayout backPlay;
    private LinearLayout competencePerso;
    private LinearLayout donsPerso;
    private LinearLayout caracterePerso;
    private Activity a;
    private ArrayList<String> inventaireList;
    private ArrayList<String> stuffList;
    private ArrayList<String> dons;
    private ArrayList<String> valeurDons;
    private ArrayList<String> competence;
    private ArrayList<String> tabcaractere;
    private String namePerso;
    @Override
    protected void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = imageDesign.getLayoutParams();
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            findViewById(R.id.tableau).setPadding(-100,0,0,0);
            findViewById(R.id.parent).setPadding(150,0,0,0);
            findViewById(R.id.layoutSocial).setPadding(30,0,0,0);
            findViewById(R.id.LayoutPhysique).setPadding(40,0,0,0);
            findViewById(R.id.base).setPadding(100,0,0,0);
            backPlay.setPadding(200,0,0,0);
            params.height = 450;
            params.width=300;
            imageDesign.setLayoutParams(params);
           if(getWindowManager().getDefaultDisplay().getWidth()>2000){
               findViewById(R.id.tableau).setPadding(-300,0,0,0);
               findViewById(R.id.parent).setPadding(500,0,0,0);
               findViewById(R.id.base).setPadding(100,0,0,0);
               backPlay.setPadding(300,0,0,0);
           }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        a.getIntent().setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.imageDesign);
            Glide.with(getApplicationContext()).load(selectedImage).into(new DrawableImageViewTarget(imageView));
            perso.setPathPhoto(selectedImage.toString());
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            sharedPreferences.edit()
                    .putString(namePerso, gson.toJson(perso))
                    .apply();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        namePerso =getIntent().getStringExtra("namePerso");
        a=this;
        backPlay = this.findViewById(R.id.backPlay);
        backPlay.setBackgroundColor(0);
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.parchemin);
        bg.setTileModeXY(Shader.TileMode.MIRROR, Shader.TileMode.MIRROR);
        backPlay.setBackgroundDrawable(bg);
        layoutInventaire = this.findViewById(R.id.layoutInventaire);
        layoutStuff = this.findViewById(R.id.layoutStuff);
        btnIncrementLife = this.findViewById(R.id.incrementLife);
        btnDecrementLife = this.findViewById(R.id.decrementLife);
        btnIncrementMana = this.findViewById(R.id.incrementMana);
        btnDecrementMana = this.findViewById(R.id.decrementMana);
        imageDesign = findViewById(R.id.imageDesign);
        selectionBase = this.findViewById(R.id.selectionBase);
        resBase = this.findViewById(R.id.resBase);
        inventaire= this.findViewById(R.id.inventaire);
        nomPerso = this.findViewById(R.id.nomPersoPlay);
        racePerso = this.findViewById(R.id.racePersoPlay);
        levelPerso = this.findViewById(R.id.niveauPersoPlay);
        caracterePerso = this.findViewById(R.id.caracterePersoPlay);
        elementPerso = this.findViewById(R.id.elementPersoPlay);
        pvPerso = this.findViewById(R.id.pvPersoPlay);
        manaPerso = this.findViewById(R.id.manaPersoPlay);
        physiquePerso = this.findViewById(R.id.physiquePersoPlay);
        socialPerso = this.findViewById(R.id.socialPersoPlay);
        mentalPerso = this.findViewById(R.id.mentalPersoPlay);
        puissancePerso = this.findViewById(R.id.puissancePersoPlay);
        finessePerso = this.findViewById(R.id.finessePersoPlay);
        auraPerso = this.findViewById(R.id.auraPersoPlay);
        relationPerso = this.findViewById(R.id.relationPersoPlay);
        instinctPerso = this.findViewById(R.id.instinctPersoPlay);
        savoirPerso = this.findViewById(R.id.savoirPersoPlay);
        competencePerso = this.findViewById(R.id.competencePersoPlay);
        donsPerso = this.findViewById(R.id.donsPersoPlay);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String json = sharedPreferences.getString(namePerso,null);
        if(json != null){
            perso = gson.fromJson(json, Caracter.class);
            remplissageFichePerso(perso);
        }

        imageDesign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertPerso = new AlertDialog.Builder(a);
                ImageView image = new ImageView(a.getApplicationContext());
                LinearLayout laytou = new LinearLayout(a.getApplicationContext());
                image.setImageURI(Uri.parse(perso.getPathPhoto()));
                Glide.with(getApplicationContext()).load(Uri.parse(perso.getPathPhoto())).into(new DrawableImageViewTarget(image));
                alertPerso.setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                laytou.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
                laytou.addView(image);
                alertPerso.setView(laytou);
                alertPerso.show();
            }
        });

        findViewById(R.id.btnDesign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        findViewById(R.id.lanceBase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectionBase.getText().toString().equals("")){
                    Random rand = new Random();
                    int res = rand.nextInt(Integer.parseInt(selectionBase.getText().toString()))+1;
                    resBase.setText(String.valueOf(res)+"/"+selectionBase.getText().toString());
                }
            }
        });
        findViewById(R.id.base6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                int res = rand.nextInt(6)+1;
                resBase.setText(String.valueOf(res)+"/6");
            }
        });
        findViewById(R.id.base8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                int res = rand.nextInt(8)+1;
                resBase.setText(String.valueOf(res)+"/8");
            }
        });
        findViewById(R.id.base10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                int res = rand.nextInt(10)+1;
                resBase.setText(String.valueOf(res)+"/10");
            }
        });
        findViewById(R.id.base12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                int res = rand.nextInt(12)+1;
                resBase.setText(String.valueOf(res)+"/12");
            }
        });
        findViewById(R.id.textDons).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutDons();
            }
        });
        findViewById(R.id.textCompetence).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutCompetence();
            }
        });
        btnDecrementLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Life("-");
            }
        });
        btnIncrementLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Life("+");
            }
        });
        btnDecrementMana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mana("-");
            }
        });
        btnIncrementMana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mana("+");
            }
        });
        findViewById(R.id.physique).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanceDeeStatsBasique();
            }
        });
        findViewById(R.id.social).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanceDeeStatsBasique();
            }
        });
        findViewById(R.id.mental).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanceDeeStatsBasique();
            }
        });

        inventaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutInventaire();
            }
        });
        findViewById(R.id.textPuissance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Puissance",".");
            }
        });
        findViewById(R.id.textFinesse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Finesse",".");
            }
        });
        findViewById(R.id.textAura).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Aura",".");
            }
        });
        findViewById(R.id.textRelation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Relation",".");
            }
        });
        findViewById(R.id.textInstinct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Instinct",".");
            }
        });
        findViewById(R.id.textSavoir).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStatSecondaire("Savoir",".");
            }
        });
        findViewById(R.id.textCaractere).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutCaractere();
            }
        });
        findViewById(R.id.stuff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajoutstuff();
            }
        });
        findViewById(R.id.btnLevelUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                levelUp(".");
            }
        });
    }

    public void levelUp(final String error){

    final AlertDialog.Builder alertLevelUp = new AlertDialog.Builder(a);
    alertLevelUp.setMessage("LEVEL UP!!!");
    LinearLayout layoutPrincipal = new LinearLayout(alertLevelUp.getContext());
    LinearLayout layoutPv=new LinearLayout(alertLevelUp.getContext());
    LinearLayout layoutMana = new LinearLayout(alertLevelUp.getContext());
    LinearLayout layoutPhysique = new LinearLayout(alertLevelUp.getContext());
    LinearLayout layoutSocial = new LinearLayout(alertLevelUp.getContext());
    LinearLayout layoutMental = new LinearLayout(alertLevelUp.getContext());
    TextView pvActuel = new TextView(alertLevelUp.getContext());
    pvActuel.setText("Pv actuel :"+perso.getPvMax()+" ");
    TextView manaActuel = new TextView(alertLevelUp.getContext());
    manaActuel.setText("Mana actuel : "+perso.getPsyMax()+" ");
    TextView physiqueActuel = new TextView(alertLevelUp.getContext());
    physiqueActuel.setText("Physique actuel : "+perso.getPhysique()+" ");
    final TextView socialActuel = new TextView(alertLevelUp.getContext());
    socialActuel.setText("Social actuel : "+perso.getSocial()+" ");
    TextView mentalActuel = new TextView(alertLevelUp.getContext());
    mentalActuel.setText("Mental Actuel : "+perso.getMental()+" ");
    TextView pvVoulut = new TextView(alertLevelUp.getContext());
    pvVoulut.setText("Pv à ajouter : ");
    final EditText pvEntrer = new EditText(alertLevelUp.getContext());
    pvEntrer.setInputType(InputType.TYPE_CLASS_NUMBER);
    TextView manaVoulut = new TextView(alertLevelUp.getContext());
    manaVoulut.setText("Mana à ajouter : ");
    final EditText manaEntrer = new EditText(alertLevelUp.getContext());
    manaEntrer.setInputType(InputType.TYPE_CLASS_NUMBER);
    TextView physiqueVoulut = new TextView(alertLevelUp.getContext());
    physiqueVoulut.setText("Physique à ajouter : ");
    final EditText physiqueEntrer = new EditText(alertLevelUp.getContext());
    physiqueEntrer.setInputType(InputType.TYPE_CLASS_NUMBER);
    TextView socialVoulut = new TextView(alertLevelUp.getContext());
    socialVoulut.setText("Social à ajouter : ");
    final EditText socialEntrer = new EditText(alertLevelUp.getContext());
    socialEntrer.setInputType(InputType.TYPE_CLASS_NUMBER);
    TextView mentalVoulut = new TextView(alertLevelUp.getContext());
    mentalVoulut.setText("Mental à ajouter :");
    final EditText mentalEntrer = new EditText(alertLevelUp.getContext());
    mentalEntrer.setInputType(InputType.TYPE_CLASS_NUMBER);
    layoutPv.addView(pvActuel);
    layoutPv.addView(pvVoulut);
    layoutPv.addView(pvEntrer);
    layoutMana.addView(manaActuel);
    layoutMana.addView(manaVoulut);
    layoutMana.addView(manaEntrer);
    layoutPhysique.addView(physiqueActuel);
    layoutPhysique.addView(physiqueVoulut);
    layoutPhysique.addView(physiqueEntrer);
    layoutSocial.addView(socialActuel);
    layoutSocial.addView(socialVoulut);
    layoutSocial.addView(socialEntrer);
    layoutMental.addView(mentalActuel);
    layoutMental.addView(mentalVoulut);
    layoutMental.addView(mentalEntrer);
    layoutPrincipal.addView(layoutPv);
    layoutPrincipal.addView(layoutMana);
    layoutPrincipal.addView(layoutPhysique);
    layoutPrincipal.addView(layoutSocial);
    layoutPrincipal.addView(layoutMental);
    layoutPrincipal.setOrientation(LinearLayout.VERTICAL);
    alertLevelUp.setNegativeButton("Fermé", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    });
    alertLevelUp.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String errorBis = error;
            String finaleError = "";
            if (!pvEntrer.getText().toString().equals("")||
                    !manaEntrer.getText().toString().equals("")||
                    !physiqueEntrer.getText().toString().equals("")||
                    !socialEntrer.getText().toString().equals("")||
                    !mentalEntrer.getText().toString().equals("")){

                if (!pvEntrer.getText().toString().equals("")) {
                    if (Integer.parseInt(pvEntrer.getText().toString())<=3){
                        if (!manaEntrer.getText().toString().equals("")){
                            if ((Integer.parseInt(manaEntrer.getText().toString()) + Integer.parseInt(pvEntrer.getText().toString())) <= 3){
                                int pvmax = perso.getPvMax();
                                perso.setPvMax(pvmax + Integer.parseInt(pvEntrer.getText().toString()));
                                perso.setPv(pvmax + Integer.parseInt(pvEntrer.getText().toString()));
                                errorBis = "";
                            }else{
                                if (!errorBis.equals("")){
                                    finaleError+="- trop de pv on été mis \n";
                                }
                            }
                        }else if (manaEntrer.getText().toString().equals("")){
                            int pvmax = perso.getPvMax();
                            perso.setPvMax(pvmax + Integer.parseInt(pvEntrer.getText().toString()));
                            perso.setPv(pvmax + Integer.parseInt(pvEntrer.getText().toString()));
                            errorBis = "";
                        }
                        else{
                            if (!errorBis.equals("")){
                                finaleError+="- trop de pv on été mis \n";
                            }

                        }
                    }
                    else{
                        if (!errorBis.equals("")){
                            finaleError+="- trop de pv on été mis \n";
                        }
                    }
                }
                if (!manaEntrer.getText().toString().equals("")) {
                    if (Integer.parseInt(manaEntrer.getText().toString())<=3){
                        if (!pvEntrer.getText().toString().equals("")){
                            if ((Integer.parseInt(pvEntrer.getText().toString()) + Integer.parseInt(manaEntrer.getText().toString())) <= 3){
                                int psymax = perso.getPsyMax();
                                perso.setPsyMax(psymax + Integer.parseInt(manaEntrer.getText().toString()));
                                perso.setPsy(psymax + Integer.parseInt(manaEntrer.getText().toString()));
                                errorBis = "";
                            }else{
                                if (!errorBis.equals("")){
                                    finaleError+="- trop de mana a été mis \n";
                                }
                            }
                        }
                        else if (pvEntrer.getText().toString().equals("")){
                            int psymax = perso.getPsyMax();
                            perso.setPsyMax(psymax + Integer.parseInt(manaEntrer.getText().toString()));
                            perso.setPsy(psymax + Integer.parseInt(manaEntrer.getText().toString()));
                            errorBis = "";
                        }
                        else{
                            if (!errorBis.equals("")){
                                finaleError+="- trop de mana a été mis \n";
                            }
                        }
                    }
                    else{
                        if (!errorBis.equals("")){
                            finaleError+="- trop de mana a été mis \n";
                        }
                    }
                }
                if (!physiqueEntrer.getText().toString().equals("")){
                    if ((perso.getPhysique()+Integer.parseInt(physiqueEntrer.getText().toString()))<=80){
                        int physiqueAdd = perso.getPhysique();
                        perso.setPhysique(physiqueAdd+Integer.parseInt(physiqueEntrer.getText().toString()));
                        errorBis = "";
                    }
                    else{
                        if (!errorBis.equals("")){
                            finaleError+="- tu ne peut pas monter plus ta physique \n";
                        }
                    }
                }
                if (!socialEntrer.getText().toString().equals("")){
                    if (perso.getSocial()+Integer.parseInt(socialEntrer.getText().toString())<=80){
                        int socialAdd = perso.getSocial();
                        perso.setSocial(socialAdd+Integer.parseInt(socialEntrer.getText().toString()));
                        errorBis = "";
                    }else{
                        if (!errorBis.equals("")){
                            finaleError+="- tu ne peut pas monter plus ta social \n";
                        }
                    }
                }
                if (!mentalEntrer.getText().toString().equals("")){
                    if (perso.getMental()+Integer.parseInt(mentalEntrer.getText().toString())<=80){
                        int mentalAdd = perso.getMental();
                        perso.setMental(mentalAdd+Integer.parseInt(mentalEntrer.getText().toString()));
                        errorBis = "";

                    }else{
                        if (!errorBis.equals("")){
                            finaleError+="- tu ne peut pas monter plus ton mental \n";
                        }
                    }
                }
                if(errorBis.equals("")){
                    int lvl = perso.getLevel()+1;
                    perso.setNiveau(lvl);
                }
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                Gson gson = new Gson();

                sharedPreferences.edit()
                        .putString(namePerso,gson.toJson(perso))
                        .apply();
                remplissageFichePerso(perso);
            }
            if (!finaleError.equals("")){
                levelUp(".");
                Toast.makeText(Play.this, finaleError, Toast.LENGTH_LONG).show();
            }
        }
    });
    alertLevelUp.setView(layoutPrincipal);
    alertLevelUp.show();
}

    public void lanceDeeStatsBasique(){
        final AlertDialog.Builder Alert = new AlertDialog.Builder(a);
        Random rand = new Random();
        int res = rand.nextInt(100);
        Alert.setMessage("tu a fait : "+String.valueOf(res)+"/100");
        Alert.show();
    }

    public void ajoutCaractere(){
        final AlertDialog.Builder Alert = new AlertDialog.Builder(a);
        final  EditText valeur = new EditText(a);
        Alert.setMessage("ajout d'un trait de caractère");
        Alert.setView(valeur);
        Alert.setPositiveButton("ajout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<String> newCara = perso.getCaractere();
                if (!valeur.getText().toString().equals("")){
                    newCara.add(valeur.getText().toString());
                    perso.setCaractere(newCara);
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                    Gson gson = new Gson();

                    sharedPreferences.edit()
                            .putString(namePerso,gson.toJson(perso))
                            .apply();
                    remplissageFichePerso(perso);
                }else{
                    dialog.dismiss();
                }
            }
        });
        Alert.show();
    }

    public void setStatSecondaire(String stats,String error){
        final AlertDialog.Builder Alert = new AlertDialog.Builder(a);
        final  EditText valeur = new EditText(a);
        valeur.setInputType(InputType.TYPE_CLASS_NUMBER);
        Alert.setView(valeur);
        if(!error.equals(".")){
            Toast.makeText(Play.this,error,Toast.LENGTH_LONG).show();
        }
        switch (stats){
            case "Puissance":
                if(perso.getPuissance()+perso.getFinesse()>=perso.getPhysique()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else{
                        Alert.setMessage("Choisissez combien ajouter à la puissance");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if ((perso.getPuissance()+Integer.parseInt(valeur.getText().toString()))+perso.getFinesse()>perso.getPhysique()/10){
                                    setStatSecondaire("Puissance","tu a ajouté trop de puissance");
                                    dialog.dismiss();
                                }else {
                                    perso.setPuissance(perso.getPuissance() + Integer.parseInt(valeur.getText().toString()));
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();
                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            }
                        });
                }
                Alert.show();
                break;
            case "Finesse":
                if(perso.getPuissance()+perso.getFinesse()>=perso.getPhysique()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else{
                        Alert.setMessage("Choisissez combien ajouter à la Finesse");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if ((perso.getPuissance()+Integer.parseInt(valeur.getText().toString()))+perso.getFinesse()>perso.getPhysique()/10){
                                    setStatSecondaire("Finesse","tu a ajouté trop de Finesse");
                                }else {
                                perso.setFinesse(perso.getFinesse() + Integer.parseInt(valeur.getText().toString()));
                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                Gson gson = new Gson();

                                sharedPreferences.edit()
                                        .putString(namePerso, gson.toJson(perso))
                                        .apply();
                                remplissageFichePerso(perso);
                            }
                            }
                        });
                }
                Alert.show();
                break;
            case "Aura":
                if(perso.getAura()+perso.getRelation()>=perso.getSocial()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else{
                        Alert.setMessage("Choisissez combien ajouter à l'Aura");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                if((perso.getAura()+Integer.parseInt(valeur.getText().toString()))+perso.getRelation()>perso.getSocial()/10){
                                            setStatSecondaire("Aura","tu a ajouté trop d'Aura");
                                }else{
                                    perso.setAura(perso.getAura() + Integer.parseInt(valeur.getText().toString()));
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            }
                        });
                }
                Alert.show();
                break;
            case "Relation":
                if(perso.getAura()+perso.getRelation()>=perso.getSocial()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else {
                        Alert.setMessage("Choisissez combien ajouter à la Relation");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if((perso.getAura()+Integer.parseInt(valeur.getText().toString()))+perso.getRelation()>perso.getSocial()/10){
                                    setStatSecondaire("Relation","tu a ajouté trop de points en Relation");
                                }else {
                                    perso.setRelation(perso.getRelation() + Integer.parseInt(valeur.getText().toString()));
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            }
                        });
                }
                Alert.show();
                break;
            case "Instinct":
                if(perso.getInstinct()+perso.getSavoir()>=perso.getMental()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else{
                        Alert.setMessage("Choisissez combien ajouter à l'Instinct");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if((perso.getInstinct()+Integer.parseInt(valeur.getText().toString()))+perso.getSavoir()>perso.getMental()/10){
                                    setStatSecondaire("Instinct","tu a ajouté trop d'Instinct");
                                }else {
                                    perso.setInstinct(perso.getInstinct() + Integer.parseInt(valeur.getText().toString()));
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            }
                        });
                }
                Alert.show();
                break;
            case "Savoir":
                if(perso.getInstinct()+perso.getSavoir()>=perso.getMental()/10){
                    Alert.setView(null);
                    Alert.setMessage("Action Impossible");
                }else {
                        Alert.setMessage("Choisissez combien ajouter au Savoir");
                        Alert.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if((perso.getInstinct()+Integer.parseInt(valeur.getText().toString()))+perso.getSavoir()>perso.getMental()/10){
                                    setStatSecondaire("Savoir","tu a ajouté trop de Savoir");
                                }else {
                                    perso.setSavoir(perso.getSavoir() + Integer.parseInt(valeur.getText().toString()));
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            }
                        });
                }
                Alert.show();
                break;
        }
    }

    public void ajoutDons(){
       final AlertDialog.Builder Alert = new AlertDialog.Builder(a);

        Alert.setView(a.getLayoutInflater().inflate(R.layout.add_dons,null));
        Alert.setMessage("Entrez le don et sa valeur");
        Alert.setPositiveButton("ajout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText don = ((AlertDialog)dialog).findViewById(R.id.ajoutDon);
                EditText valeur = ((AlertDialog)dialog).findViewById(R.id.ajoutDonvaleur);
                ArrayList<String> dons = perso.getDons();
                ArrayList<String> valeurdons = perso.getValeurDons();
                if (!don.getText().toString().equals("")){
                    dons.add(don.getText().toString());
                    valeurdons.add(valeur.getText().toString());
                    perso.setDons(dons);
                    perso.setValeurDons(valeurdons);
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                    Gson gson = new Gson();

                    sharedPreferences.edit()
                            .putString(namePerso,gson.toJson(perso))
                            .apply();
                    remplissageFichePerso(perso);
                }

            }
        });
        Alert.show();
    }

    public void ajoutCompetence(){
        final AlertDialog.Builder Alert = new AlertDialog.Builder(a);

        Alert.setView(a.getLayoutInflater().inflate(R.layout.add_skill,null));
        Alert.setMessage("Entrez la compétence");
        Alert.setPositiveButton("ajout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText competenceText = ((AlertDialog)dialog).findViewById(R.id.addSkill);
                ArrayList<String> competence = perso.getCompetences();
                competence.add(competenceText.getText().toString());
                perso.setCompetences(competence);
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                Gson gson = new Gson();

                sharedPreferences.edit()
                        .putString(namePerso,gson.toJson(perso))
                        .apply();
                remplissageFichePerso(perso);
            }
        });
        Alert.show();
    }

    public void Life(String symbol){

        if (symbol.equals("+")&&perso.getPv()<perso.getPvMax()) {
            pvPerso.setText(perso.getPv()+1+"/"+perso.getPvMax());
            int pv = perso.getPv()+1;
            perso.setPv(pv);
            Caracter newPerso = perso;
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            Gson gson = new Gson();

            sharedPreferences.edit()
                    .putString(namePerso,gson.toJson(newPerso))
                    .apply();
        }else if (symbol.equals("-")&&perso.getPv()>perso.getPvMax()-(perso.getPvMax()*2)){
            pvPerso.setText(perso.getPv()-1+"/"+perso.getPvMax());
            int pv = perso.getPv()-1;
            perso.setPv(pv);
            Caracter newPerso = perso;
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            Gson gson = new Gson();

            sharedPreferences.edit()
                    .putString(namePerso,gson.toJson(newPerso))
                    .apply();
            if (perso.getPv()==perso.getPvMax()-(perso.getPvMax()*2)){
                final AlertDialog.Builder Alert = new AlertDialog.Builder(a);
                Alert.setMessage("                 Tu est Mort(e)");
                Alert.show();
            }
        }
    }

    public void Mana(String symbol){
        if (symbol.equals("+")&&perso.getPsy()<perso.getPsyMax()){
                manaPerso.setText(perso.getPsy()+1+"/"+perso.getPsyMax());
                perso.setPsy(perso.getPsy()+1);
            Caracter newPerso = perso;
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            Gson gson = new Gson();

            sharedPreferences.edit()
                    .putString(namePerso,gson.toJson(newPerso))
                    .apply();
        }else if (symbol.equals("-")&&perso.getPsy()>0){
            manaPerso.setText(perso.getPsy()-1+"/"+perso.getPsyMax());
            perso.setPsy(perso.getPsy()-1);
            Caracter newPerso = perso;
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
            Gson gson = new Gson();

            sharedPreferences.edit()
                    .putString(namePerso,gson.toJson(newPerso))
                    .apply();
            if (perso.getPsy()==0){
                final AlertDialog.Builder Alert = new AlertDialog.Builder(a);
                Alert.setMessage("            Utilise tes points de vie");
                Alert.show();
            }
        }
    }

    public void remplissageFichePerso(final Caracter perso){
            if (perso!=null) {
                nomPerso.setText(perso.getNom());
                racePerso.setText(perso.getRace());
                levelPerso.setText(String.valueOf(perso.getLevel()));
                tabcaractere = perso.getCaractere();
                caracterePerso.removeAllViews();
                for (int i = 0; i < tabcaractere.size(); i++) {
                    LinearLayout layout = new LinearLayout(a);
                    TextView trait = new TextView(a);
                    ImageView btnRemove = new ImageView(a);
                    final int remove = i;
                    btnRemove.setImageURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + getResources().getResourcePackageName(R.drawable.moin)
                            + '/' + getResources().getResourceTypeName(R.drawable.moin) + '/' + getResources().getResourceEntryName(R.drawable.moin)));
                    layout.setPadding(0, 20, 0, 0);
                    trait.setPadding(0, 0, 20, 0);
                    btnRemove.setPadding(0, 0, 0, 0);
                    trait.setText("- " + tabcaractere.get(i));
                    trait.setTextSize(30);
                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tabcaractere.remove(remove);
                            perso.setCaractere(tabcaractere);
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                            Gson gson = new Gson();

                            sharedPreferences.edit()
                                    .putString(namePerso, gson.toJson(perso))
                                    .apply();
                            remplissageFichePerso(perso);
                        }
                    });
                    if (perso.getPathPhoto() != null) {
                        Glide.with(getApplicationContext()).load(perso.getPathPhoto()).into(new DrawableImageViewTarget(imageDesign));
                    }
                    layout.addView(trait);
                    layout.addView(btnRemove);
                    layout.setOrientation(LinearLayout.HORIZONTAL);
                    ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                    params.height = 70;
                    params.width = 70;
                    btnRemove.setLayoutParams(params);
                    caracterePerso.addView(layout);
                }
                elementPerso.setText(perso.getElement());
                pvPerso.setText(String.valueOf(perso.getPv()) + "/" + String.valueOf(perso.getPvMax()));
                manaPerso.setText(String.valueOf(perso.getPsy()) + "/" + String.valueOf(perso.getPsyMax()));
                physiquePerso.setText("         " + String.valueOf(perso.getPhysique()));
                socialPerso.setText("     " + String.valueOf(perso.getSocial()));
                mentalPerso.setText("      " + String.valueOf(perso.getMental()));
                puissancePerso.setText(String.valueOf(perso.getPuissance()));
                finessePerso.setText(String.valueOf(perso.getFinesse()));
                auraPerso.setText(String.valueOf(perso.getAura()));
                relationPerso.setText(String.valueOf(perso.getRelation()));
                instinctPerso.setText(String.valueOf(perso.getInstinct()));
                savoirPerso.setText(String.valueOf(perso.getSavoir()));
                competence = perso.getCompetences();
                competencePerso.removeAllViews();
                for (int i = 0; i < competence.size(); i++) {
                    LinearLayout layout = new LinearLayout(a);
                    TextView compet = new TextView(a);
                    ImageView btnRemove = new ImageView(a);
                    final int remove = i;
                    btnRemove.setImageURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + getResources().getResourcePackageName(R.drawable.moin)
                            + '/' + getResources().getResourceTypeName(R.drawable.moin) + '/' + getResources().getResourceEntryName(R.drawable.moin)));
                    layout.setPadding(0, 20, 0, 0);
                    compet.setPadding(0, 0, 20, 0);
                    btnRemove.setPadding(0, 0, 0, 0);
                    if (!competence.get(i).equals("")) {
                        compet.setText("- " + competence.get(i));
                        compet.setTextSize(30);
                        layout.addView(compet);
                        layout.addView(btnRemove);
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                        params.height = 70;
                        params.width = 70;
                        btnRemove.setLayoutParams(params);
                        competencePerso.addView(layout);
                    } else {
                        competence.remove(i);
                        perso.setCompetences(competence);
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                        Gson gson = new Gson();

                        sharedPreferences.edit()
                                .putString(namePerso, gson.toJson(perso))
                                .apply();
                        remplissageFichePerso(perso);
                    }

                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            competence.remove(remove);
                            perso.setCompetences(competence);
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                            Gson gson = new Gson();

                            sharedPreferences.edit()
                                    .putString(namePerso, gson.toJson(perso))
                                    .apply();
                            remplissageFichePerso(perso);
                        }
                    });
                }
                dons = perso.getDons();
                valeurDons = perso.getValeurDons();
                donsPerso.removeAllViews();
                for (int i = 0; i < dons.size(); i++) {
                    LinearLayout layout = new LinearLayout(a);
                    TextView don = new TextView(a);
                    ImageView btnRemove = new ImageView(a);
                    final int remove = i;
                    btnRemove.setImageURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                            "://" + getResources().getResourcePackageName(R.drawable.moin)
                            + '/' + getResources().getResourceTypeName(R.drawable.moin) + '/' + getResources().getResourceEntryName(R.drawable.moin)));
                    layout.setPadding(0, 20, 0, 0);
                    don.setPadding(0, 0, 20, 0);
                    btnRemove.setPadding(0, 0, 0, 0);
                    if (!valeurDons.get(i).equals("")) {
                        don.setText("- " + dons.get(i) + " : " + valeurDons.get(i) + "%");
                        don.setTextSize(30);
                        layout.addView(don);
                        layout.addView(btnRemove);
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                        params.height = 70;
                        params.width = 70;
                        btnRemove.setLayoutParams(params);
                        donsPerso.addView(layout);
                    } else if (valeurDons.get(i).equals("")) {
                        if (dons.get(i).equals("") && valeurDons.get(i).equals("")) {
                            dons.remove(i);
                            valeurDons.remove(i);
                            perso.setDons(dons);
                            perso.setValeurDons(valeurDons);
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                            Gson gson = new Gson();

                            sharedPreferences.edit()
                                    .putString(namePerso, gson.toJson(perso))
                                    .apply();
                            remplissageFichePerso(perso);
                        } else {
                            don.setTextSize(30);
                            don.setText("- " + dons.get(i));
                            layout.addView(don);
                            layout.addView(btnRemove);
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                            params.height = 70;
                            params.width = 70;
                            btnRemove.setLayoutParams(params);
                            donsPerso.addView(layout);
                        }
                    }

                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dons.remove(remove);
                            valeurDons.remove(remove);
                            perso.setDons(dons);
                            perso.setValeurDons(valeurDons);
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                            Gson gson = new Gson();

                            sharedPreferences.edit()
                                    .putString(namePerso, gson.toJson(perso))
                                    .apply();
                            remplissageFichePerso(perso);
                        }
                    });
                }
                layoutInventaire.removeAllViews();
                inventaireList = perso.getInventaire();
                if (inventaireList != null) {
                    if (inventaireList.size() != 0) {
                        for (int i = 0; i < inventaireList.size(); i++) {
                            TextView newStuff = new TextView(a);
                            LinearLayout lay = new LinearLayout(a);
                            lay.setOrientation(LinearLayout.HORIZONTAL);
                            ImageView btnRemove = new ImageView(a);
                            final int remove = i;
                            btnRemove.setImageURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                                    "://" + getResources().getResourcePackageName(R.drawable.moin)
                                    + '/' + getResources().getResourceTypeName(R.drawable.moin) + '/' + getResources().getResourceEntryName(R.drawable.moin)));
                            lay.setPadding(0, 20, 0, 0);
                            newStuff.setPadding(0, 0, 20, 0);
                            btnRemove.setPadding(0, 0, 0, 0);
                            btnRemove.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    inventaireList.remove(remove);
                                    perso.setInventaire(inventaireList);
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            });
                            newStuff.setText("- " + inventaireList.get(i));
                            newStuff.setTextSize(30);
                            lay.addView(newStuff);
                            lay.addView(btnRemove);
                            ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                            params.height = 70;
                            params.width = 70;
                            btnRemove.setLayoutParams(params);
                            layoutInventaire.addView(lay);
                        }
                    }
                }
                layoutStuff.removeAllViews();
                stuffList = perso.getStuff();
                if (stuffList != null) {
                    if (stuffList.size() != 0) {
                        for (int i = 0; i < stuffList.size(); i++) {
                            TextView newStuff = new TextView(a);
                            LinearLayout lay = new LinearLayout(a);
                            lay.setOrientation(LinearLayout.HORIZONTAL);
                            ImageView btnRemove = new ImageView(a);
                            final int remove = i;
                            btnRemove.setImageURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                                    "://" + getResources().getResourcePackageName(R.drawable.moin)
                                    + '/' + getResources().getResourceTypeName(R.drawable.moin) + '/' + getResources().getResourceEntryName(R.drawable.moin)));
                            lay.setPadding(0, 20, 0, 0);
                            newStuff.setPadding(0, 0, 20, 0);
                            btnRemove.setPadding(0, 0, 0, 0);
                            btnRemove.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    stuffList.remove(remove);
                                    perso.setStuff(stuffList);
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                                    Gson gson = new Gson();

                                    sharedPreferences.edit()
                                            .putString(namePerso, gson.toJson(perso))
                                            .apply();
                                    remplissageFichePerso(perso);
                                }
                            });
                            newStuff.setText("- " + stuffList.get(i));
                            newStuff.setTextSize(30);
                            lay.addView(newStuff);
                            lay.addView(btnRemove);
                            ViewGroup.LayoutParams params = btnRemove.getLayoutParams();
                            params.height = 70;
                            params.width = 70;
                            btnRemove.setLayoutParams(params);
                            layoutStuff.addView(lay);
                        }
                    }
                }
            }
        overrideFonts(getApplicationContext(),backPlay);
    }

    public void ajoutInventaire(){
        final AlertDialog.Builder ajoutInventaire = new AlertDialog.Builder(a);
        ajoutInventaire.setMessage("Ressource à ajouter");
        ajoutInventaire.setView(a.getLayoutInflater().inflate(R.layout.ajout_inventaire,null));
        ajoutInventaire.setPositiveButton("ajout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText ajout = ((AlertDialog)dialog).findViewById(R.id.ajoutInventaire);
                if (!ajout.getText().toString().equals("")){
                    inventaireList.add(ajout.getText().toString());
                    perso.setInventaire(inventaireList);
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                    Gson gson = new Gson();

                    sharedPreferences.edit()
                            .putString(namePerso,gson.toJson(perso))
                            .apply();
                    remplissageFichePerso(perso);
                }else{
                    dialog.dismiss();
                }

            }
        });
        ajoutInventaire.show();
    }

    public void ajoutstuff(){
        final AlertDialog.Builder ajoutStuff = new AlertDialog.Builder(a);
        ajoutStuff.setView(a.getLayoutInflater().inflate(R.layout.ajout_inventaire,null));
        ajoutStuff.setMessage("Item à ajouter");
        ajoutStuff.setPositiveButton("ajout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText ajout = ((AlertDialog)dialog).findViewById(R.id.ajoutInventaire);
                if (!ajout.getText().toString().equals("")){
                    stuffList.add(ajout.getText().toString());
                    perso.setStuff(stuffList);
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);
                    Gson gson = new Gson();

                    sharedPreferences.edit()
                            .putString(namePerso,gson.toJson(perso))
                            .apply();
                    remplissageFichePerso(perso);
                }else{
                    dialog.dismiss();
                }

            }
        });
        ajoutStuff.show();
    }

    public static void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(),"customfont.otf"));
            }
        } catch (Exception e) {
        }
    }
}
