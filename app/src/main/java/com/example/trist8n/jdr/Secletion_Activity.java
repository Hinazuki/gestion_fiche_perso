package com.example.trist8n.jdr;
//This is not openSource,if you want to use this source for publishing on store contact me on Trist8n.56@hotmail.fr.
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Secletion_Activity extends Activity {
    private ArrayList<String> listePerso;
    private LinearLayout layoutPrincipal;
    private LinearLayout layoutSelec;
    private Context ctx;
    private Button btnRetour;
    private Pattern pattern;
    private Matcher matcher;
    @Override
    protected void onResume() {
        super.onResume();
        layoutPrincipal.removeAllViews();
        Actualisation();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secletion_);
        layoutPrincipal = this.findViewById(R.id.layoutPrincipal);
        layoutPrincipal.removeAllViews();
        layoutSelec = this.findViewById(R.id.layoutSelec);
        overrideFonts(getApplicationContext(),layoutSelec);
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.parchemin);
        bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
        layoutSelec.setBackgroundDrawable(bg);
        btnRetour = this.findViewById(R.id.btnRetour);
        ctx = this.getApplicationContext();
        Actualisation();
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void Actualisation(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);

        String json = sharedPreferences.getString("listePerso",null);

        if(json != null){
            pattern = Pattern.compile("\"([a-zA-Z])+.([a-zA-Z])+\"");
            matcher = pattern.matcher(json);
            listePerso = new ArrayList<>();
            while(matcher.find()){
                listePerso.add(json.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1));
            }
            if (!matcher.find()){
                pattern = Pattern.compile("\"([a-zA-Z])\"");
                matcher = pattern.matcher(json);
                while(matcher.find()){
                    listePerso.add(json.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1));
                }
            }
            for(int i=0;i<listePerso.size();i++){
                final LinearLayout layout = new LinearLayout(this.getApplicationContext());
                final LinearLayout layoutPerso = new LinearLayout(this.getApplicationContext());
                TextView btnPerso = new TextView(this.getApplicationContext());
                ImageView btnDelet = new ImageView(this.getApplicationContext());
                ImageView imagePerso = new ImageView(this.getApplicationContext());
                String perso = sharedPreferences.getString(listePerso.get(i),null);
                Caracter persoSelect;
                Gson gson = new Gson();
                if(json != null){
                    persoSelect = gson.fromJson(perso, Caracter.class);
                    btnPerso.setText(listePerso.get(i)+"\n lvl: "+persoSelect.getLevel()+"    Race: "+persoSelect.getRace());
                    if (persoSelect.getPathPhoto()!=null){
                        imagePerso.setImageURI(Uri.parse(persoSelect.getPathPhoto()));
                        Glide.with(getApplicationContext()).load(persoSelect.getPathPhoto()).into(new DrawableImageViewTarget(imagePerso));
                    }
                }
                final int finalI = i;
                layoutPerso.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent Test = new Intent(ctx, Play.class);
                        Test.putExtra("namePerso",listePerso.get(finalI));
                        startActivity(Test);
                    }
                });
                btnDelet.setImageResource(R.drawable.poubelle);
                btnDelet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listePerso.remove(finalI);
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PREFS", Context.MODE_PRIVATE);

                        Gson gsonList = new Gson();

                        sharedPreferences.edit()
                                .putString("listePerso",gsonList.toJson(listePerso))
                                .apply();
                        layout.removeAllViews();
                        layoutPrincipal.removeView(layout);
                    }
                });

                btnPerso.setPadding(0,20,0,0);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layoutPerso.addView(imagePerso);
                layoutPerso.addView(btnPerso);
                layoutPerso.setPadding(0,0,200,0);
                layout.addView(layoutPerso);
                layout.addView(btnDelet);
                overrideFonts(getApplicationContext(),layout);
                ViewGroup.LayoutParams params = btnDelet.getLayoutParams();
                params.height = 150;
                params.width = 120;
                btnDelet.setLayoutParams(params);
                params = imagePerso.getLayoutParams();
                params.height = 150;
                params.width = 120;
                imagePerso.setLayoutParams(params);
                layoutPrincipal.addView(layout);
            }
        }
    }
    public static void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTextSize(25);
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(),"customfont.otf"));
            }
        } catch (Exception e) {
        }
    }
}
