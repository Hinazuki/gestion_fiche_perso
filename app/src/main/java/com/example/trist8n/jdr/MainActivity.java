package com.example.trist8n.jdr;
//This is not openSource,if you want to use this source for publishing on store contact me on Trist8n.56@hotmail.fr.
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
    private LinearLayout backMain;
    private static final int PERMISSIONS_REQUEST_READ_PICTURE= 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backMain = this.findViewById(R.id.backMain);
        overrideFonts(getApplicationContext(),backMain);
        BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.parchemin);
        backMain.setBackgroundDrawable(bg);
        Button btnCreat = this.findViewById(R.id.creatPerso);
        Button play = this.findViewById(R.id.play);
        final Context ctx = this.getApplicationContext();
        final int permissionCheck = ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PICTURE);
        }
        btnCreat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Test = new Intent(ctx, CreatCaracter.class);
                startActivity(Test);
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Test = new Intent(ctx, Secletion_Activity.class);
                startActivity(Test);
            }
        });
    }
    public static void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(),"customfont.otf"));
            }
        } catch (Exception e) {
        }
    }
}
